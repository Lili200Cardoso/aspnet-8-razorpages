using Microsoft.AspNetCore.Mvc.RazorPages;
using Razorpages.Data;

namespace Razorpages.Pages.Movies;

public class IndexModel(MoviesContext moviesContext) : PageModel
{
    public ICollection<Movie> Movies { get; private set; }
    public void OnGet()
    {
        Movies = moviesContext.Movies.ToList();
    }
}
