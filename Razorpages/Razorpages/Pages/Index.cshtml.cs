using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Razorpages.Pages
{
    public class IndexModel : PageModel
    {
        public string Title { get; set; }
        public void OnGet()
        {
            Title = "My APP from IndexModel";
        }
    }
}
